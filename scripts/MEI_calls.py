#!/usr/bin/env python
import sys 
from transpac.io.RepeatMaskerIO import *
from Bio import SeqIO
from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()
 
fn2 = SeqIO.parse(open(sys.argv[2]),"fasta")
table = open(sys.argv[3],"r")

tsds = {}
seqs = {}
transeq ={}
 

for i in fn2:
	seqs[i.id]=str(i.seq)

for line in table:
	sl = line.strip().split()
	seqid = sl[0]+"_I"
	tsd1 = sl[2]
	tsd2 = sl[4]
	tsd_ref = sl[6]
	if similar(tsd1,tsd_ref)>0.85 and similar(tsd2,tsd_ref)>0.85 and len(tsd1)>4 and len(tsd1)<45:
		tsds[seqid] = sl
	

rmes = parseRepeatMasker(sys.argv[1],seqs)
for rme in rmes:
	if rme.maybeMEI() is True:
		name = rme.query_id
 		if (name in seqs) and (name in tsds):
			transeq[name] = [">"+name,seqs[name]]
			print rme

