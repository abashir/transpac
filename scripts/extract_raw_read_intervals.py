#!/usr/bin/env python
import os 
import sys
import pysam

def get_read_intervals (bamfile, me, chrom, start, end, fafile,qout,rout):
    # hack to get just sequence
    rseq = "".join(pysam.faidx(fafile,"%s:%s-%s" % (chrom, start,end)).split("\n")[1:])
    for raw_read in bamfile.fetch(chrom, start, end):
        
        # check start and end
        tstart, tend = raw_read.reference_start, raw_read.reference_end
        if not (tstart < start and tend > end):
            continue
        
        #print "*****************%s*******************" %(raw_read.query_name)
        # try and get start and end positions in query
        qstart = None
        qend = None
        for ap in raw_read.get_aligned_pairs():
            qpos,rpos = ap
            #print qpos, rpos, start, end, qstart, qend
            if rpos is not None and rpos < start and qpos is not None:
                #print "yo"
                qstart = qpos
            if rpos is not None and rpos < end and qpos is not None:
                #print "yo2"
                qend = qpos
            elif rpos is not None and rpos >= end and qpos is not None:
                #print "yo3"
                pass
        
        # check if coordinates returned otherwise fail
        assert qstart is not None
        assert qend   is not None
        
        # get seq
        qseq = raw_read.query_sequence[qstart:qend]
        
        print >>qout, ">%s_%s_%s_%i_%i_%i_%i\n%s" %(raw_read.query_name, me, chrom, start, end, qstart, qend, qseq)
        print >>rout, ">%s_%s_%s_%i_%i_%i_%i\n%s" %(raw_read.query_name, me, chrom, start, end, qstart, qend, rseq)

    


infn = sys.argv[1]
fafn = sys.argv[2]
coordfn = sys.argv[3]
flank = int(sys.argv[4])
bamfile = pysam.AlignmentFile(infn, "rb")
 

# out files         
qout = open(sys.argv[5], 'w')
rout = open(sys.argv[6], 'w')

with open(coordfn) as f:
    for l in f:
        # chrom:start-end
        #chrom,se = l.strip().split(":")
        l = l.strip()
        chrom = l.split("\t")[0]
        if chrom[0:3] != "chr" or chrom == "chrhs37d5":
            continue        
        chrom, me, start, end = l.split("\t")
        start,end = int(start), int(end)
        get_read_intervals(bamfile, me, chrom, start-flank, end+flank, fafn, qout, rout)

        
