#!/bin/sh

BAM= $1
regions= $2
flanking_size= $3
k= $4
Ref= $5
Path2RepeatMasker= $6
species= $7
outdir= $8

mkdir -p ${outdir}/sequences/queries
mkdir -p ${outdir}/sequences/ref
mkdir -p ${outdir}/tables
mkdir -p ${outdir}/seq_in_tsd        
 
extract_raw_read_intervals.py $BAM \
${Ref} \
$regions \ 
$flanking_size \
${outdir}/sequences/queries/${BAM}_query.fa \
${outdir}/sequences/ref/${BAM}_ref.fa

aligner.py ${outdir}/sequences/queries/${BAM}_query.fa ${outdir}/sequences/ref/${BAM}_ref.fa $k --fmt table > ${outdir}/tables/${BAM}_${k}.table     
aligner.py  ${outdir}/sequences/queries/${BAM}_query.fa ${outdir}/sequences/ref/${BAM}_ref.fa $k --fmt ins_fasta > ${outdir}/seq_in_tsd/${BAM}_${k}_seq_in_tsd.fa

####change_id 
mkdir -p ${outdir}/change_id
mkdir -p ${outdir}/change_id/keys
mkdir -p ${outdir}/change_id/sequence
change_id_with_keys.py create_key \
${outdir}/seq_in_tsd/${BAM}_${k}_seq_in_tsd.fa \
${outdir}/change_id/keys/${BAM}_${k}_seq_in_tsd.key \
${outdir}/change_id/sequence/${BAM}_${k}_seq_in_tsd.fa

#RepeatMakser screening
mkdir -p ${outdir}/repeatmasker
cd ${outdir}/repeatmasker 
$Path2RepeatMasker/RepeatMasker -pa 12 -species ${species} ${outdir}/change_id/sequence/${BAM}_${k}_seq_in_tsd.fa

mkdir -p ${outdir}/old_id_sequence
change_id_with_keys.py use_key ${outdir}/change_id/sequence/${BAM}_${k}_seq_in_tsd.fa.out \
${outdir}/change_id/keys//${BAM}_${k}_seq_in_tsd.key \
${outdir}/old_id_sequence/${BAM}_${k}_seq_in_tsd.fa.out
  

mkdir -p ${outdir}/mei
MEI_call.py \
${outdir}/old_id_sequence/${BAM}_${k}_seq_in_tsd.fa.out \
${outdir}/seq_in_tsd/${BAM}_${k}_in_tsd.fa \
${outdir}/tables/${BAM}_${k}.table > ${outdir}/mei/${BAM}_${k}.mei


