#!/bin/sh 
set -e -x
####USAGE
####bash transpac_assembly.sh query.fa target.fa k Path2RepeatMasker species Reference NThread

on_minerva=False # Make True if on Mount Sinai minerva

mkdir -p TSDFinder
mkdir -p TSDFinder/MEI 
mkdir -p TSDFinder/transduction

query=$1
target=$2
k=$3
Path2RepeatMasker=$4
species=$5  
Ref=$6
NThread=$7

aligner.py $query $target $k --fmt table > TSDFinder/${query}_TSDFinder_${k}.table
aligner.py $query $target $k --fmt ins_fasta > TSDFinder/${query}_TSDFinder_${k}_ins.fa


cd TSDFinder
####repeatMasker screening
${Path2RepeatMasker}/RepeatMasker -species $species  ${query}_TSDFinder_${k}_ins.fa

### Calling MEI
MEI_calls.py ${query}_TSDFinder_${k}_ins.fa.out  ${query}_TSDFinder_${k}_ins.fa  ${query}_TSDFinder_${k}.table > MEI/${query}.MEI

### Calling transduction 
transduction_calls.py ${query}_TSDFinder_${k}_ins.fa.out  ${query}_TSDFinder_${k}_ins.fa  ${query}_TSDFinder_${k}.table > transduction/${query}_transduction_candidates

candidates=`grep -c . transduction/${query}_transduction_candidates`
if [ "$candidates" -eq "0" ]; 
then
    echo "No transduction candidates found. Exiting."
    exit 0
fi

cd transduction

# Because Minerva python libraries and smrtanalysis dont work together                                                                                                                                      
if [ "${on_minerva}" == "True" ]
then
    module purge
    module load smrtanalysis
fi

if [ -f $Ref.sa ]
then
    echo "Suffix array found for $Ref"
    echo "blasr -noSplitSubreads -m 4 -clipping soft  ${query}_TSDFinder_${k}_ins.fa_ME-transduction.fa $Ref -sa $Ref.sa -out  blasr_${query}_TSDFinder_${k}_ins.fa.m4 -nproc $NThread"
    blasr -noSplitSubreads -m 4 -clipping soft  ${query}_TSDFinder_${k}_ins.fa_ME-transduction.fa $Ref -sa $Ref.sa -out  blasr_${query}_TSDFinder_${k}_ins.fa.m4 -nproc $NThread
else
    echo "No suffix array file found."
    blasr -noSplitSubreads -m 4 -clipping soft  ${query}_TSDFinder_${k}_ins.fa_ME-transduction.fa  $Ref -out  blasr_${query}_TSDFinder_${k}_ins.fa.m4 -nproc $NThrea
fi

echo "blasr step complete."

if [ "${on_minerva}" == "True" ]
then
    module purge
    module load python/2.7.6 py_packages
fi

#check whether the candidates mapped successfully to the reference genome. If not, exit gracefully.
hits=`grep -c . blasr_${query}_TSDFinder_${k}_ins.fa.m4`
if [ "$hits" -eq "0" ];
then
    echo "Of the " $candidates " transduction candidates, none mapped to the reference genome. Exiting."
    exit 0
fi

echo "parsing blasr outputs for full length transductions."

blasr_full_length.py blasr_${query}_TSDFinder_${k}_ins.fa.m4 > blasr_${query}_TSDFinder_${k}_ME-transduction.FL
echo "Done parsing blasr outputs for full length transductions."
