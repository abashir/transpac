# README #

### TransPac overview ###

TransPac: transposon detection and characterization from long-reads

### How do I get set up? ###

## Dependencies
### Packages
```
python/2.7.6
numpy
Cython
biopython
h5py
```
### Tools
```
BEDTools
RepeatMasker
samtools
blasr
```
Additionally, any components required for RepeatMasker, such as Crossmatch, ABBLAST/WUBLAST, HMMER

## Install
```
git clone https://bitbucket.org/abashir/transpac.git
cd transpac
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
python setup.py install
```
## Note
Please change your path to blasr on bash script transpac_assembly.sh. 
No need to change the path for minerva user

## Running example
```
cd example
bash ../transpac_assembly.sh query_1_test.fa target_1_test.fa 50 /Path/to/RepeatMasker/  human  Path/To/Refs/ucsc.hg19.fasta  32
```

## Using transpac for assembly data
```
bash transpac_assembly.sh query.fa target.fa k Path2RepeatMasker species Reference NThread 
```
The required input to transpac_assembly.sh is:

1. `query.fa`: Query sequence from assembly. (format: fasta)
2. `target.fa`: Sequence from reference spanning the boundary. (format: fasta)
3. `k`: Maximal TSD size. (format: integer)
4. `Path2RepeatMasker`: Please download RepeatMasker and write its full path. (format: /path/to/RepeatMasker/)
5. `species`: Species for RepeatMasker screening. (format: species included by RepeatMasker, i.e. human)
6. `Reference`: Please download your reference genome and specify the sequence file with full path. (format: /path/to/genome.fa)
7. `NThread`: Number of processors allowed. (format: integer)
 
## Using transpac for raw reads
```
bash transpac_rawreads.sh BAM regions flanking_size k Reference Path2RepeatMasker species outdir
```
The required input to transpac_rawreads.sh is:

1. `BAM`: A set of raw reads to extract sequence.(format: bam file)
2. `regions`: A list of region of interest(i.e. insertion point. (format: chrom'\t'ME'\t'start'\t'end)
3. `flanking_size`: Length of addtional sequencing flanking the break point. (format: integer, i.e.100)
4. `k`: Maximal TSD size. (format: integer)
5. `Reference`: Please download your reference genome and specify the sequence file with full path. (format: /path/to/genome.fa)
6. `Path2RepeatMasker`: Please download RepeatMasker and write its full path.(format: /path/to/RepeatMasker/)
7. `species`: Species for RepeatMasker screening. (format: species included by RepeatMasker, i.e. human)
8. `ourdir`: Name of output directory (format: character, i.e. Raw).
 
### Who do I talk to? ###
* xintong.chen@icahn.mssm.edu
