
from setuptools import setup, find_packages
import numpy
from Cython.Build import cythonize

include_path = [numpy.get_include()]
setup(
    name='transpac',
    url='https://chenx08@bitbucket.org/abashir/transpac.git',
    description='Testing',
    packages=find_packages(),
    include_package_data=True,
    scripts = ["scripts/MEI_calls.py",
               "scripts/aligner.py",
               "scripts/blasr_full_length.py",
               "scripts/extract_raw_read_intervals.py",
	       "scripts/change_id_with_keys.py",
               "scripts/transduction_calls.py"],
    entry_points={
        'console_scripts': [ 'transpac = transpac.transpac:main' ]
        },
    ext_modules = cythonize("transpac/cython/*.pyx",include_path),
    include_dirs=[numpy.get_include()],
    platforms='any'
)


