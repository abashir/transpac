import sys
import os
from transpac.io.FastaIO import FastaReader
from difflib import SequenceMatcher

'''
fn2 = sys.argv[2]
fastaseqs = {}
for entry in FastaReader(fn2):
	fastaseqs[entry.name] = entry.sequence
'''

class RepeatMaskerElement:
    
    def __init__ (self, line, fastaseqs=None):
        """
        Ex:
        bit   perc perc perc  query                   position in query    matching      repeat             position in repeat
        score   div. del. ins.  sequence                begin end   (left)   repeat        class/family     begin  end    (left)   ID
        
        148    1.4  0.0  0.0  10_100306895_100306896     17   154    (4) + AluYa8        SINE/Alu            169    306    (4)    1  

        """
        
        values = line.strip().split()
        self.score = int(values[0])
        self.div, self.dels, self.ins   = map(float, values[1:4])
        self.query_id = values[4]
        self.query_start, self.query_end = map(int, values[5:7])
        self.query_remaining = int(values[7][1:-1])
        self.query_length = self.query_end + self.query_remaining
        self.strand = values[8]
        if fastaseqs is not None:
            self.qseq = fastaseqs[self.query_id]       
        else:
            self.qseq = None
        if self.strand == "C":
            self.strand = "-"
            self.target_start = int(values[13])
            self.target_end   = int(values[12])
            self.target_remaining = int(values[11][1:-1])
        else:
            self.target_start = int(values[11])
            self.target_end   = int(values[12])
            self.target_remaining = int(values[13][1:-1])
        self.target_id = values[9]
        self.repeat_class  = values[10]
        self.target_length = self.target_end + self.target_remaining

    def __str__ (self):
        return "\t".join(map(str, [self.query_id, self.query_start, self.query_end, self.query_length, self.target_id, self.target_start, self.target_end, self.target_length, self.strand, self.repeat_class, self.qseq]))
    
    def isFullLength (self, missed=50):
        if self.target_start < missed and self.target_remaining < missed:
            return True
        return False

    def is3primeComplete (self, missed=50):
        # the right side is complete
        if self.target_remaining < missed:
            return True
        return False

    def is5primeComplete (self, missed=50):
        # the left side is complete
        if self.target_start < missed:
            return True
        return False

    def is3or5primeComplete (self, missed=50):
        if self.is5primeComplete(missed) or self.is3primeComplete(missed):
            return True
        return False
        
    def mainlyElement (self, flank=25):
        '''
        Checks to see if the sequence is mainly a mobile element. 
        The flank is the allowed "non ME" sequence tolerated upstream
        and downstream of the mobile element
        E.g.
           5'                                                   3'

        ~~~~~~~(ME ----------------------------------------->)~~~~~~
        
        if the len(~~~~~~~) sequence is > flank then this will return 
        false.  E.g. the inserted sequence is NOT solely a mobile element.
        For example, could be a transduction.
i
        Whereas this:

        ~~(ME ----------------------->)~~
        
        or

        (ME ----------------------->)~~~

        might not be valid transductions
        
        '''
        if self.query_start < flank and self.query_remaining < flank:
            return True
        else:
            return False

    def isTransduceable (self, flank=25):
	if self.repeat_class == "Retroposon/SVA" and self.is3or5primeComplete(100):
            return True
	elif self.repeat_class == "LINE/L1" and self.is3primeComplete(100):
            return True
	elif self.repeat_class == "SINE/Alu" and  self.isFullLength(50):
            return True
        return False

    def maybeMEI (self,variable_seq =10):
	if self.repeat_class == "Retroposon/SVA" or  self.repeat_class == "LINE/L1" or self.repeat_class == "SINE/Alu":
		if self.query_start < variable_seq or (self.query_length-self.query_end) < variable_seq:
			return True
	return False

    def isTransduction (self, flank=25, min_trans_len=15,variable_seq=10, ATfrac=0.8 ):
        if self.isTransduceable(flank):
            if self.repeat_class == "Retroposon/SVA":
                # ---------------( -------->   full elength SVA   ) ---------------#
		# ---------------( <--------   full elength SVA   ) ---------------#
		#       ULEN                                             DLEN
		# where either the upstream and/or dowstreams flanks are transduced
		# i.e. DLEN > min_trans_len or ULEN > min_trans_len
		if not (self.mainlyElement(min_trans_len)) and self.isFullLength(100):
			transeq = self.qseq[0:self.query_start+1] + self.qseq[self.query_end-1:self.query_length+1]
			if (transeq.count('A')/float(len(transeq)) < ATfrac or transeq.count('T')/float(len(transeq)) < ATfrac) and len(transeq) >15: 
				return TRUE
		elif not (self.mainlyElement(min_trans_len)) and self.is5primeComplete(100):
			# --------------( 5prime SVA -------->)#
			if self.strand =="+" and (self.query_length-self.query_end) < variable_seq:
				transeq = self.qseq[0:self.query_start+1]
				if transeq.count('A')/float(len(transeq)) < ATfrac and len(transeq)>15:
					return True
			# (<------ 5prime SVA)-----------------#
			elif self.strand == "-" and self.query_start < variable_seq:
				transeq = self.qseq[self.query_end-1:self.query_length+1]
				if  transeq.count('T')/float(len(transeq)) < ATfrac and len(transeq)>15:
					return True
                elif not (self.mainlyElement(min_trans_len)) and self.is3primeComplete(100):
                        # (-------->3prime SVA)---------------#
                    	if self.strand =="+" and self.query_start < variable_seq:
				transeq = self.qseq[self.query_end-1:self.query_length]
				if transeq.count('A')/float(len(transeq)) < ATfrac  and len(transeq)>15:
					if transeq[-8:].count('A') > 4 :
						return True
                        # --------------( 3prime SVA <--------)#
			elif self.strand == "-" and (self.query_length-self.query_end)  < variable_seq:
				transeq = self.qseq[0:self.query_start]
				if transeq.count('T')/float(len(transeq)) < ATfrac and len(transeq)>15:
					if transeq[0:8].count('T') > 4:
						return True
            elif not (self.mainlyElement(min_trans_len)):
		# (-------->3prime ALU/L1)---------------#
		if self.strand =="+" and self.query_start < variable_seq:
			transeq = self.qseq[self.query_end-1:self.query_length]
			if transeq.count('A')/float(len(transeq)) < ATfrac and len(transeq)>15:
				if transeq[-8:].count('A') > 4 :
					return True
		# --------------( 3prime ALU/L1 <--------)#
		elif self.strand == "-" and (self.query_length-self.query_end)  < variable_seq:
			transeq = self.qseq[0:self.query_start]
                        #print >>sys.stderr, transeq
			if  transeq.count('T')/float(len(transeq)) < ATfrac and len(transeq)>15:
                                #print >>sys.stderr, "first if"
				if transeq[:8].count('T') > 4:
                                        #print >>sys.stderr, "second if"
					return True
        return False


def parseRepeatMasker (fn,fastaseqs=None):
    with open (fn) as f:
        f.readline()
        f.readline()
        f.readline()
        for l in f:
            rme = RepeatMaskerElement(l,fastaseqs)
            yield rme
 

def runRepeatMasker (repeatmaskerpath, fasta_fn, rm_fn, nproc):
    os.system("rm %s" %(rm_fn))
    rmcmd = "%s -species human -q -pa %i %s > %s" %(repeatmaskerpath, nproc, fasta_fn, rm_fn)
    print >>sys.stderr, rmcmd
    os.system(rmcmd)
    
    
    

def fill_mei_trs_sats_from_RepeatMasker (fn, meiDict, trDict, satDict, nameDict, rm_fn, meifrac=0.8, trfrac=0.8, log=False):
    '''
    run nhmmscan and parse output
    '''
    for rme in parseRepeatMasker (rm_fn):
        (tid, t_s, t_e, qid, q_s, q_e, seqlen) = nameDict[rme.query_id]
        elen = abs(rme.query_end-rme.query_start)
        currfrac = float(elen)/seqlen
        etup = (tid, t_s, t_e, qid)
        tname = rme.target_id
        if currfrac > trfrac and (rme.repeat_class == "Simple_repeat" or rme.repeat_class == "Low_complexity"):
            trDict[etup] = "DUP:TANDEM"

        elif currfrac > trfrac and "Satellite" in rme.repeat_class:
            satDict[etup] = "INS:SATELLITE"
       
        elif currfrac > meifrac:
            # check Alu
            if tname[0:3] == "Alu" and elen > 100:
                meiDict[etup] = "INS:ME:Alu"                    
            # check SVA
            elif tname[0:3] == "SVA" and elen > 500:
                meiDict[etup] = "INS:ME:SVA"
                print >>sys.stderr, tname, elen, seqlen, currfrac
                print >>sys.stderr, str(rme)
            # check L1
            elif tname[0:2] == "L1" and elen > 500:
                meiDict[etup] = "INS:ME:L1"
            else:
                #meiDict[etup] = "INS:ME:%s" %(rme.target_id)
                meiDict[etup] = "INS:ME:OTH_OR_PARTIAL"
    return trDict, meiDict, satDict           
    
if __name__ == "__main__":
	fn2 = sys.argv[2]
	fastaseqs = {}
	for entry in FastaReader(fn2):
        	fastaseqs[entry.name] = entry.sequence	
        rmiter = parseRepeatMasker(sys.argv[1],fastaseqs)
        for rme in rmiter:
                print rme

